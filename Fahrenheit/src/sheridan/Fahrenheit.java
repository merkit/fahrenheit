package sheridan;

public class Fahrenheit {

	public static void main(String[] args) {
		System.out.println(convertFromCelsius(100));

	}

	public static int convertFromCelsius(int temp) {
		return (int) Math.ceil((temp * (9/(double)5)) + 32);
	}
	
}
