package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class FahrenheitTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testConvertFromCelsiusRegular() {
		assertTrue("wrong", Fahrenheit.convertFromCelsius(0) == 32);
	}
	
	@Test
	public void testConvertFromCelsiusExceptional() {
		assertFalse("wrong", Fahrenheit.convertFromCelsius(25) == 25);
	}
	
	@Test
	public void testConvertFromCelsiusBoundaryIn() {
		assertTrue("wrong", Fahrenheit.convertFromCelsius(12) == 54);
	}
	
	@Test
	public void testConvertFromCelsiusBoundaryOut() {
		assertFalse("wrong", Fahrenheit.convertFromCelsius(12) == 53);
	}

}
